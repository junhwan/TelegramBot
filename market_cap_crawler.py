import requests
from bs4 import BeautifulSoup
import time

class MarketCapCrawler:
    cache = {}
    
    def __init__(self):
        self.symbolIdMap = {}
        self.avgPriceMap = {}
        tickerJson = self.getTicker()
        for tickerData in tickerJson:
            if tickerData["symbol"] not in self.symbolIdMap:
                self.symbolIdMap[tickerData["symbol"]] = tickerData["id"]
                self.avgPriceMap[tickerData["symbol"]] = tickerData["price_usd"]

    def getMarketCapId(self, coin):
        if coin not in self.symbolIdMap:
            return "INVALID"
        return self.symbolIdMap[coin]
    
    def getAveragePrice(self, coin):
        if coin not in self.avgPriceMap:
            return 0
        return float(self.avgPriceMap[coin])

    def getTicker(self):
        TICKER_API_URL = "https://api.coinmarketcap.com/v1/ticker/?limit=0"
        # req = requests.get(TICKER_API_URL)
        req = self.cachedRequest(TICKER_API_URL, 21600) # 6hrs
        return req.json()
    
    def getMarketUri(self, id):
        return "https://coinmarketcap.com/currencies/" + id + "/#markets"
    
    # returns [[ #, exchange, pair, volume($), price, volume(%), updated ], ... ]
    def parseMarketPage(self, uri):
        req = self.cachedRequest(uri)
        soup = BeautifulSoup(req.text, "html.parser")

        resData = []
        for tr in soup.find(id="markets-table").tbody.find_all("tr"):
            resData.append(map(filterTd, tr.find_all("td")))
        return resData
    
    def cachedRequest(self, uri, cacheInterval = 600):
        currentTimestamp = time.time()
        if uri in MarketCapCrawler.cache:
            cacheData = MarketCapCrawler.cache[uri]
            if currentTimestamp - cacheData[0] <= cacheInterval:
                return cacheData[1]
        req = requests.get(uri)
        MarketCapCrawler.cache[uri] = [currentTimestamp, req]
        return req

    # id = bittrex, upbit, bithumb, coinone
    def getExchangeUri(self, id):
        return "https://coinmarketcap.com/exchanges/" + id

    # returns { currency: [ pair, price-usd, price-native ], ... }
    def parseExchangePage(self, uri):
        req = self.cachedRequest(uri)
        soup = BeautifulSoup(req.text, "html.parser")

        resData = {}
        for tr in soup.find(id="exchange-markets").tbody.find_all("tr"):
            tdArr = tr.find_all("td")
            key = tdArr[1].find("a").get("href").split("/")[2] # currencyId
            if key in resData:
                continue
            value = [
                tdArr[2].get("data-sort"),
                float(tdArr[4].find("span").get("data-usd")),
                float(tdArr[4].find("span").get("data-native"))]
            resData[key] = value
        return resData

def filterTd(td):
    if "***" in td.getText():
        return "-1"
    return td.get("data-sort") or td.getText()
