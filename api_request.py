import requests;

# SECRET = "CMbot213";

def call(url, method, data={}):
    # params["telegram_bot_secret"] = SECRET;
    print("Requesting: " + url)
    try:
        if method == "POST":
            response = requests.post(url, data=data)
        elif method == "GET":
            response = requests.get(url)
        print(response.text)
    except requests.exceptions.RequestException as e:
        print e;
        return None;
    return response;
