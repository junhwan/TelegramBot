from market_cap_crawler import MarketCapCrawler
import api_request as ApiRequest
import time

CM_LINK_URL = "https://fz3vw.app.goo.gl/CM"
EXCHANGE_RATE_URL = "https://coinmanager.io/api/service/exchange_rate?query="

def getExchangePriceMap(coin, coinMarketData):
    coinMarketData.sort(key=lambda e: float(e[3]), reverse=True)
    exchangePriceMap = {}
    dataLength = len(coinMarketData)
    for idx, dataArray in enumerate(coinMarketData): # iterate numExchange / 2 times
        if idx > dataLength / 3: break
        exchange = dataArray[1]
        pair = dataArray[2].split("/")
        if coin != pair[1] and exchange not in exchangePriceMap and isValid(dataArray):
            price = dataArray[4]
            exchangePriceMap[exchange] = price
    return exchangePriceMap

def getMaxMinPair(exchangePriceMap):
    maxPair = [None, float("-inf")]
    minPair = [None, float("inf")]
    for exchange, price in exchangePriceMap.items():
        price = float(price)
        if price > maxPair[1]:
            maxPair[0] = exchange
            maxPair[1] = price
        if price < minPair[1]:
            minPair[0] = exchange
            minPair[1] = price
    return {"min": minPair, "max": maxPair}

def isValid(dataArray):
    return dataArray[6] == "Recently"

def getPremium(coin):
    marketCapCrawler = MarketCapCrawler()
    marketCapId = marketCapCrawler.getMarketCapId(coin)
    if marketCapId == "INVALID":
        return None
    marketCapPageURI = marketCapCrawler.getMarketUri(marketCapId)
    coinMarketData = marketCapCrawler.parseMarketPage(marketCapPageURI)
    maxMinDict = getMaxMinPair(getExchangePriceMap(coin, coinMarketData))
    maxMinDict["avg"] = marketCapCrawler.getAveragePrice(coin)
    return maxMinDict

cache = {}
def getExchangeRate(_from, _to):
    currentTimestamp = time.time()
    cacheKey = _from + "-" + _to
    if cacheKey in cache:
        if currentTimestamp - cache[cacheKey]["timestamp"] <= 60 * 60:
            return cache[cacheKey]["value"]

    exchangeRateUrl = EXCHANGE_RATE_URL + _from + "-" + _to
    res = ApiRequest.call(exchangeRateUrl, "GET")
    json = res.json()
    exchangeJson = json["data"]
    cache[_from + "-" + _to] = {
        "value": exchangeJson["query"]["results"]["rate"]["Rate"],
        "timestamp": time.time()
    }
    return exchangeJson["query"]["results"]["rate"]["Rate"]


def getPremiumText(coin, maxPair, minPair, avgPrice=1):
    text = "Premium " + coin + "\n"
    text += "*Average Price*\n"
    text += "$ " + str("{:,}".format(round(avgPrice,3))) + "\n"
    text += "*" + maxPair[0] + "* (HIGH)\n"
    priceStr = "$ " + str("{:,}".format(round(maxPair[1],3)))
    diffStr = str("{:+2.2f}".format((maxPair[1] / avgPrice - 1) * 100)) + "%"
    text += "{0:<18}".format(priceStr) + "_" + diffStr + "_\n"
    text += "*" + minPair[0] + "* (LOW)\n"
    priceStr = "$ " + str("{:,}".format(round(minPair[1],3)))
    diffStr = str("{:+2.2f}".format((minPair[1] / avgPrice - 1) * 100)) + "%"
    text += "{0:<18}".format(priceStr) + "_" + diffStr + "_\n"
    text += "[See Details](" + CM_LINK_URL + ")"

    # tempText = "{0: <18}".format(maxPair[0])
    # text = "Premium " + coin + "\nHIGH - _" + tempText + "_" + str(round(maxPair[1],3)) + " USD\nLOW - _"
    # tempText = "{0: <19}".format(minPair[0])
    # text += tempText + "_" + str(round(minPair[1],3)) + " USD\n"
    # if avgPrice > 0:
    #     text += "PRICE                          " + str(round(avgPrice, 3)) + " USD\n"
    # text += "Difference *" + str(round(maxPair[1] / minPair[1] * 100 - 100, 2)) + "%*\n" 
    # text += "[See Details](" + CM_LINK_URL + ")"
    return text


def getKrPremium(coin, exchanges):
    marketCapCrawler = MarketCapCrawler()
    currencyId = marketCapCrawler.getMarketCapId(coin)
    if currencyId == "INVALID":
        return None
    priceDict = {}
    for exchangeId in exchanges:
        marketCapPageURI = marketCapCrawler.getExchangeUri(exchangeId)
        currencyPriceMap = marketCapCrawler.parseExchangePage(marketCapPageURI)
        if currencyId in currencyPriceMap:
            pairPriceArr = currencyPriceMap[currencyId]
            if pairPriceArr[0].split("/")[1] in ["USD", "KRW"]:
                priceDict[exchangeId] = currencyPriceMap[currencyId]
    
    priceDict["average"] = marketCapCrawler.getAveragePrice(coin)
    return priceDict
