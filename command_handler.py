# -*- coding: utf8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import traceback
import premium_helper as PremiumHelper
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from simple_logger import SimpleLogger
import api_request as ApiRequest
import market_cap_crawler as MarketCapCrawler

logger = SimpleLogger()

LINK_URL = "https://fz3vw.app.goo.gl/CM"
REQUEST_LINK_URL = "https://goo.gl/forms/Df6CrUs7643vLMhB3"

def helpHandler(bot, update):
    logger.info("got help command, chat id:" + str(update.message.chat.id))
    text = """
        코인매니저 챗봇입니다.
/prem 명령어로 코인의 *프리미엄*을 확인하실 수 있습니다.
/krp 명령어로 국내 거래소의 코인 프리미엄을 확인하실 수 있습니다.
사용 예시 : /prem BTC
기능 개발 요청하기 : /request

CoinManager chatbot.
You can check the premium(spread) between exchanges  with the /prem command.
Ex. /prem BTC
To request additional feature development: /request"""

    keyboard = [[InlineKeyboardButton("See Details", callback_data='1', url=LINK_URL)]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_markdown(text, reply_markup=reply_markup)

def requestHandler(bot, update, args):
    REQUEST_API_URL = "https://coinmanager.io/api/request/post"
    try:
        userId = update.message.from_user.id
        if not args:
            update.message.reply_text("요청하실 내용과 함께 입력해주세요.")
            logger.info("got request command without args, user id:" + str(userId))
            return
        content = " ".join(args)
        logger.info("got request command, user id:" + str(userId))
        data = {
            "text": content,
            "user_id": userId
        }
        ApiRequest.call(REQUEST_API_URL, "POST", data)
        text = """
        요청하신 내용이 접수되었습니다."""

        update.message.reply_markdown(text)
    except Exception,e:
        print(e)

def premiumHandler(bot, update, args):
    try:
        if not args:
            update.message.reply_text("코인과 함께 입력해주세요.\nPlease input with coin.\nex. /prem BTC")
            logger.info("got prem command without args, chat id: " + str(update.message.chat.id))
            return
            
        logger.info("got prem command: " + args[0] + ", chat id:" + str(update.message.chat.id))
        coin = args[0].upper()
        maxMinDict = PremiumHelper.getPremium(coin)
        if maxMinDict is None:
            update.message.reply_text("Coin not found.\n코인을 찾을 수 없습니다.")
            return
        if "avg" in maxMinDict:
            text = PremiumHelper.getPremiumText(coin, maxMinDict["max"], maxMinDict["min"], maxMinDict["avg"])
        else:
            text = PremiumHelper.getPremiumText(coin, maxMinDict["max"], maxMinDict["min"])
        update.message.reply_markdown(text, disable_web_page_preview=True)
        return
    except Exception,e:
        # traceback.print_exc()
        logger.error(e)

def krPremiumHandler(bot, update, args):
    try:
        if not args:
            update.message.reply_text("코인과 함께 입력해주세요.\nPlease input with coin.\nex. /krp BTC")
            logger.info("got krprem command without args, chat id: " + str(update.message.chat.id))
            return

        logger.info("got krprem command: " + args[0] + ", chat id:" + str(update.message.chat.id))
        coin = args[0].upper()
        exchanges = ["bitfinex", "upbit", "bithumb", "coinone"]
        baseExchange = exchanges[0]
        priceDict = PremiumHelper.getKrPremium(coin, exchanges)
        if priceDict is None:
            update.message.reply_text("코인을 찾을 수 없습니다.\nCoin not found.")
            return
        if baseExchange in priceDict:
            basePrice = priceDict[baseExchange][1]
        else:
            basePrice = priceDict["average"]
            baseExchange = "Average"
    
        basePriceKRW = basePrice * PremiumHelper.getExchangeRate("USD", "KRW")
        text = "Korean Premium " + coin + "\n"
        text += "*" + baseExchange.title() + "*\n"
        text += "₩ " + "{0:<14,}".format(int(basePriceKRW)) + "($ " + str("{:,}".format(basePrice)) + ")\n"
        for i in range(1,4):
            if exchanges[i] in priceDict:
                price = priceDict[exchanges[i]][2]
                text += "*" + exchanges[i].title() + "*\n"
                text += "₩ " + str("{0:<17,}".format(int(price))) + "_" + str("{:+2.2f}".format((price / basePriceKRW - 1) * 100)) + "%_\n"

        # text += "_{0: <17}_".format(baseExchange.title()) + "₩ " + str("{:,}".format(int(basePriceKRW))) + "   ($ " + str("{:,}".format(basePrice)) + ")\n"
        # for i in range(1,4):
        #     if exchanges[i] in priceDict:
        #         price = priceDict[exchanges[i]][2]
        #         text += "_{0: <17}_".format(exchanges[i].title()) + "₩ " + str("{:,}".format(int(price))) + "        *" + str("{:+2.2f}".format((price / basePriceKRW - 1) * 100)) + "%*\n"
        text += "[See Details](" + LINK_URL + ")"
        update.message.reply_markdown(text, disable_web_page_preview=True)
    except Exception,e:
        logger.error(e)
        # traceback.print_exc()
