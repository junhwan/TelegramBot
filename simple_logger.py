import logging
import logging.handlers
from datetime import datetime

class SimpleLogger:
    def __init__(self):
        dateTime = datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
        self.logger = logging.getLogger()
        formatter = logging.Formatter('[%(levelname)s] %(asctime)s > %(message)s')
        fileHandler = logging.FileHandler("logs/" + dateTime + ".log")
        # streamHandler = logging.StreamHandler()
        fileHandler.setFormatter(formatter)
        # streamHandler.setFormatter(formatter)
        self.logger.addHandler(fileHandler)
        # self.logger.addHandler(streamHandler)
        self.logger.setLevel(logging.INFO)
    
    def info(self, text):
        self.logger.info(text)
    
    def debug(self, text):
        self.logger.debug(text)
    
    def warning(self, text):
        self.logger.warning(text)
    
    def error(self, text):
        self.logger.error(text)
    
    def critical(self, text):
        self.logger.critical(text)
    