module.exports = shipit => {
  // Load shipit-deploy tasks
  require('shipit-deploy')(shipit);
  require("dotenv").config();

  let exec = require("child_process").exec;
  let penv = process.env;

  shipit.initConfig({
    default: {
      workspace: './deploy',
      deployTo: '/home/ubuntu/cm_bot',
      repositoryUrl: 'https://gitlab.com/junhwan/TelegramBot.git',
      ignores: [".git"],
      key: penv.PEMFILE_PATH
    },
    production: {
      servers: 'ubuntu@13.209.49.227',
    },
  })
}
