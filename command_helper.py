# -*- coding: utf8 -*-
from telegram.ext import CommandHandler, CallbackQueryHandler
import command_handler as commandHandler

def commandHelper(updater) :
    updater.dispatcher.add_handler(CommandHandler('start', commandHandler.helpHandler))
    updater.dispatcher.add_handler(CommandHandler('help', commandHandler.helpHandler))
    updater.dispatcher.add_handler(CommandHandler('prem', commandHandler.premiumHandler, pass_args = True))
    updater.dispatcher.add_handler(CommandHandler('krp', commandHandler.krPremiumHandler, pass_args = True))
    updater.dispatcher.add_handler(CommandHandler('request', commandHandler.requestHandler, pass_args = True))